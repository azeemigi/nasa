package org.labs.qbit.nasa.parser.impl;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.labs.qbit.nasa.parser.Parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ParserImpl implements Parser {

	/* (non-Javadoc)
	 * @see org.labs.qbit.nasa.parser.Parser#parse(java.lang.String)
	 */
	@Override
	public Map<String, Object> parse(String jsonString) {
		ObjectMapper mapper = new ObjectMapper(new JsonFactory());
	    TypeReference<HashMap<String,Object>> typeRef = new TypeReference<HashMap<String,Object>>() {};
	    try{
	    	HashMap<String,Object> o = mapper.readValue(jsonString, typeRef);
	    	return o;
	    }catch(IOException e){
	    	e.printStackTrace();
	    }
		return null;
	}
	
	@Override
	public Map<String, Object> parse(InputStream jsonStream) {
		ObjectMapper mapper = new ObjectMapper(new JsonFactory());
	    TypeReference<HashMap<String,Object>> typeRef = new TypeReference<HashMap<String,Object>>() {};
	    try{
	    	HashMap<String,Object> o = mapper.readValue(jsonStream, typeRef);
	    	return o;
	    }catch(IOException e){
	    	e.printStackTrace();
	    }
		return null;
	}

}
