package org.labs.qbit.nasa.parser;

import java.io.InputStream;
import java.util.Map;

public interface Parser {

    /**
     * Given the JSON string, parse it to a MAP, where key is the name and the value is the description
     *
     * @param jsonString String
     * @return Map
     */
    public Map<String, Object> parse(String jsonString);
    
    /**
     * Given the JSON Input Stream, parse it to a MAP, where key is the name and the value is the description
     *
     * @param jsonStream InputStream
     * @return Map
     */
    public Map<String, Object> parse(InputStream jsonStream);
}