package org.labs.qbit.nasa.persist.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;
import org.labs.qbit.nasa.persist.Persistence;

import java.net.UnknownHostException;

public class MongoDbPersistence implements Persistence {

    private DBCollection searchCollection;

    {
        String host = System.getenv("OPENSHIFT_MONGODB_DB_HOST");
        String sport = System.getenv("OPENSHIFT_MONGODB_DB_PORT");
        int port = Integer.decode(sport);
        String db = System.getenv("OPENSHIFT_APP_NAME");
        String user = System.getenv("OPENSHIFT_MONGODB_DB_USERNAME");
        String password = System.getenv("OPENSHIFT_MONGODB_DB_PASSWORD");
        try {
            MongoClient mongoClient = new MongoClient(host, port);
            DB database = mongoClient.getDB(db);
            boolean authenticate = database.authenticate(user, password.toCharArray());
            if (authenticate) {
                searchCollection = database.getCollection("search");
            } else {
                throw new MongoException("Authentication Failed");
            }
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean persist(String query) {
        if (query == null) {
            return false;
        }
        BasicDBObject basicDBObject = new BasicDBObject("query-string", query)
                .append("timestamp", String.valueOf(System.currentTimeMillis()));
        WriteResult writeResult = searchCollection.insert(basicDBObject);
        return writeResult.getError() == null;
    }
}
