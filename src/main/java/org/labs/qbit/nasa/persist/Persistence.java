package org.labs.qbit.nasa.persist;

public interface Persistence {

    /**
     * save the query string into the database
     *
     * @param query String
     * @return boolean (success or fail)
     */
    boolean persist(String query);
}
