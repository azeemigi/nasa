package org.labs.qbit.nasa.search;

import java.io.IOException;
import java.io.InputStream;

public interface Search {

    /**
     * given the user search query, send a GET request to NASA Data API
     * http://data.nasa.gov/api/get_search_results/?search=saturn
     *
     * @param query String
     * @return JSON String
     */
	InputStream execute(String query) throws IOException;
}
