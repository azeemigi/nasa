package org.labs.qbit.nasa.search.impl;

import org.labs.qbit.nasa.search.Search;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class SearchImpl implements Search {

    @Override
    public InputStream execute(String query) throws IOException {
        String url = "http://data.nasa.gov/api/get_search_results/?search=" + query;
        System.out.println(url);
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");
        return con.getInputStream();

    }

}
