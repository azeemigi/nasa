package org.labs.qbit.nasa;

public class Constants {

    public static final String POSTS = "posts";

    public static final String ID = "id";

    public static final String SLUG = "slug";

    public static final String TITLE = "title";

    public static final String CONTENT = "content";
}
