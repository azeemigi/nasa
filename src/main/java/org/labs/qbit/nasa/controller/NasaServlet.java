package org.labs.qbit.nasa.controller;

import org.labs.qbit.nasa.parser.Parser;
import org.labs.qbit.nasa.parser.impl.ParserImpl;
import org.labs.qbit.nasa.persist.Persistence;
import org.labs.qbit.nasa.persist.impl.MongoDbPersistence;
import org.labs.qbit.nasa.search.Search;
import org.labs.qbit.nasa.search.impl.SearchImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.labs.qbit.nasa.Constants.CONTENT;
import static org.labs.qbit.nasa.Constants.ID;
import static org.labs.qbit.nasa.Constants.POSTS;
import static org.labs.qbit.nasa.Constants.SLUG;
import static org.labs.qbit.nasa.Constants.TITLE;

public class NasaServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Search search;
    private Parser parser;
    private Persistence persistence;

    @Override
    public void init() throws ServletException {
        super.init();
        search = new SearchImpl();
        parser = new ParserImpl();
        persistence = new MongoDbPersistence();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<List<String>> resultList = new ArrayList<List<String>>();
        String keyword = req.getParameter("keyword");
        System.out.println(keyword);
        if (keyword != null) {
            InputStream inputStream = search.execute(keyword);
            Map<String,Object> map = parser.parse(inputStream);
            persistence.persist(keyword);
            List<Map<String, Object>> posts = (List<Map<String, Object>>) map.get(POSTS);
            if (posts != null) {
                for (Map<String, Object> post : posts) {
                    List<String> list = new ArrayList<String>();
                    String id = String.valueOf(post.get(ID));
                    list.add(id);
                    list.add((String) post.get(SLUG));
                    list.add((String) post.get(TITLE));
                    String content = (String) post.get(CONTENT);
                    if (content.length() >= 100) {
                        content = content.substring(0, 100);
                    }
                    list.add(content);
                    resultList.add(list);
                }
            } else {
                keyword = null;
            }
        }
        System.out.println(resultList);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/search.jsp");
        req.setAttribute("keyword", keyword == null ? "UNKNOWN" : keyword);
        req.setAttribute("resultList", resultList);
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
