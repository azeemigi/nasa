<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Nasa Search</title>
</head>
<body>
<br><br><br><br>

<form method="post" action="controller">
    <table border="0" width="350" align="center" bgcolor="#D1E6E7">
        <tr>
            <td colspan=2 style="font-size:12pt;color:#00000;" align="center">
                <h4>Search from Nasa</h4></td>
        </tr>
        <tr>
            <td><b>Key Word</b></td>
            <td>: <input type="text" name="keyword" id="keyword">
            </td>
        </tr>
        <tr>
            <td colspan=2 align="center">
                <input type="submit" name="submit" value="Search"></td>
        </tr>
    </table>
</form>
<br><br><br>
<table align="center" bgcolor="#FFFFCC" border="1" width="70%">
    <tr>
        <td colspan="2" align="center"><%="Search Query: " + request.getAttribute("keyword") %></td>
    </tr>
    <tr>
        <th>ID</th>
        <th>SLUG</th>
        <th>TITLE</th>
        <th>CONTENT</th>
    </tr>
    <%
        List<List<String>> resultList = (List<List<String>>) request.getAttribute("resultList");
        if (resultList == null) {
            resultList = new ArrayList<List<String>>();
        }
        for (List<String> aResultList : resultList) {
    %>
    <tr>
        <td>
            <%= aResultList.get(0)%>
        </td>
        <td>
            <%= aResultList.get(1)%>
        </td>
        <td>
            <%= aResultList.get(2)%>
        </td>
        <td>
            <%= aResultList.get(3)%>
        </td>
    </tr>

    <%
        }
    %>
</table>
<br><br><br>
<h3><u>Team: Itanium Solutions</u></h3>
<h4>Asanka</h4>
<h4>Azeem</h4>
<h4>Manoj</h4>
<h4>Nadeera</h4>

</body>
</html>