package org.labs.qbit.nasa.parser.impl;

import org.labs.qbit.nasa.parser.Parser;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

public class ParserImplTest {

    private Parser parser;

    @BeforeClass
    public void setUp() {
        parser = new ParserImpl();
    }

    @Test
    public void testParse() {
        String json = "{\"J\":5,\"0\":\"N\"}";
        Map<String,Object> map = parser.parse(json);
        Assert.assertEquals(String.valueOf(map.get("J")), "5");
    }
}
