package org.labs.qbit.nasa.search.impl;

import org.labs.qbit.nasa.search.Search;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SearchImplTest {

    private static final String EXPECTED_ERROR_RESPONSE =
            "{\"status\":\"error\",\"error\":\"Include 'search' var in your request.\"}";

    private Search search;

    @BeforeClass
    public void setUp() {
        search = new SearchImpl();
    }

    @Test
    public void testExecute() throws Exception {

        //Test Empty
        InputStream nullInputStream = search.execute("");
        Assert.assertNotNull(nullInputStream);
        String parse = parse(nullInputStream);
        Assert.assertNotNull(parse);
        Assert.assertEquals(parse, EXPECTED_ERROR_RESPONSE);

        //Test null
        InputStream inputStream = search.execute(null);
        Assert.assertNotNull(inputStream);
        parse = parse(inputStream);
        Assert.assertNotNull(parse);

        //Test with valid values
        inputStream = search.execute("saturn");
        Assert.assertNotNull(inputStream);
        parse = parse(inputStream);
        Assert.assertNotNull(parse);
        System.out.println(parse);
    }

    private String parse(InputStream inputStream) {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String inputLine;
        StringBuilder response = new StringBuilder();
        try {
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        } catch (IOException e) {
            Assert.fail("An exception occurred", e);
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                Assert.fail("An exception occurred while closing the buffer", e);
            }
        }
        return response.toString();
    }
}
