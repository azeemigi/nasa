package org.labs.qbit.nasa;

import org.labs.qbit.nasa.parser.Parser;
import org.labs.qbit.nasa.parser.impl.ParserImpl;
import org.labs.qbit.nasa.persist.Persistence;
import org.labs.qbit.nasa.persist.impl.MongoDbPersistence;
import org.labs.qbit.nasa.search.Search;
import org.labs.qbit.nasa.search.impl.SearchImpl;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import static org.labs.qbit.nasa.Constants.*;

public class Integration {

    private Search search;
    private Parser parser;
    private Persistence persistence;

    @BeforeClass
    public void setUp() {
        search = new SearchImpl();
        parser = new ParserImpl();
        persistence = new MongoDbPersistence();
    }

    @Test
    public void test() {
        String keyword = "sun";
        try {
            InputStream inputStream = search.execute(keyword);
            Map<String,Object> map = parser.parse(inputStream);
            persistence.persist(keyword);

            List<Map<String, Object>> posts = (List<Map<String, Object>>) map.get(POSTS);
            for (Map<String, Object> post : posts) {
                System.out.println(post.get(ID));
                System.out.println(post.get(SLUG));
                System.out.println(post.get(TITLE));
                System.out.println("=====================");
            }
            System.out.println(posts.size());
            Assert.fail();
        } catch (IOException e) {
            Assert.fail(e.getMessage(), e);
        }
    }
}
