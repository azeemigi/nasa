package org.labs.qbit.nasa.persist.impl;

import org.labs.qbit.nasa.persist.Persistence;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MongoDbPersistenceTest {

    private Persistence mongoDbPersistence;

    @BeforeClass
    public void setup() {
        try {
            mongoDbPersistence = new MongoDbPersistence();
        } catch (Exception ex) {
            Assert.fail("An Error Occurred", ex);
        }
    }

    @Test
    public void testPersist() throws Exception {
        for (int i = 0; i < 10; i++) {
            boolean persist = mongoDbPersistence.persist(String.valueOf(i));
            Assert.assertTrue(persist);
        }
    }
}
